public class Hello
{
    private int language;

    // TODO
    // Ajouter autres langues
    // Anglais  : Hello
    // Espagnol : Hola
    // Russe    : Привет
    private static String[] greeting = { "Salut", "Hello", "Hola", "Привет" };

    // TODO
    // Ajouter autres langues
    // Anglais  : 1
    // Espagnol : 2
    // Russe    : 3
    private static String[] languages = { "0 - Français ", "1 - Anglais ", "2 - Espagnol ", "3 - Russe " };

    public Hello(int language) {
        // TODO
        // Ajouter vérification d'index
        if (language < 0 || language > languages.length)
            this.language = 0;
        else
            this.language = language;
    }

    public String GetGreeting() {
        return greeting[this.language];
    }

    public void DisplayLanguages(){
        for(int i = 0; i<languages.length, i++){
            System.out.println(languages[i]);
        }
    }

    public static String[] GetAvailableLanguages() {
        return languages;
    }
}
