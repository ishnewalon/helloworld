public class HelloWorld
{
    public static void main(String[] args) {
        // Test langues disponibles
        //
        System.out.println("Langues disponibles :");
        for (String language : Hello.GetAvailableLanguages()) {
            System.out.println("  " + language);
        }

        // Test langue 1
        //
        System.out.println();
        Hello hello = new Hello(0);
        System.out.println(hello.GetGreeting());

        // TODO
        // Ajouter tests pour les autres langues

        System.out.println();
        Hello holla = new Hello(1);
        System.out.println(holla.GetGreeting());

        System.out.println();
        Hello Привет = new Hello(2);
        System.out.println(Привет.GetGreeting());
    }
}
